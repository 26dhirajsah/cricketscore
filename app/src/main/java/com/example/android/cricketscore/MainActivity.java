package com.example.android.cricketscore;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button fourButton;
    private Button SixButton;
    private Button oneButton;
    private Button wicketButton;
    private Button overButton;
    private Button ballButton;
    private TextView teamA;



    private Button fourButtontb;
    private Button SixButtontb;
    private Button oneButtontb;
    private Button wicketButtontb;
    private Button overButtontb;
    private Button ballButtontb;
    private TextView teamB;


    private ScoreViewModel viewModel;

    public static final String TAG="MainActivity";
    public static final String TEAM_A_RUN="team_a_run";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        teamA=findViewById(R.id.teamA);
        teamB=findViewById(R.id.teamB);
       /* if (savedInstanceState !=null){
            int runA=savedInstanceState.getInt(TEAM_A_RUN);
            teamARun=runA;
            teamA.setText(""+ teamARun);
        }*/
        Log.i("MainActivity","View model Provider Called");

        viewModel = ViewModelProviders.of(this).get(ScoreViewModel.class);

    }
    public void displayScoreTeamA(int score){
        teamA.setText("Team A ("+String.valueOf(score)+"/"+viewModel.teamAWicket+")   Over("+viewModel.teamAOver+"."+ viewModel.teamABall+")");
    }
    public void displayScoreTeamB(int score){
        teamB.setText("Team B ("+String.valueOf(score)+"/"+viewModel.teamBWicket+")   Over("+viewModel.teamBOver+"."+ viewModel.teamBBall+")");
    }
    public void updateFourRunTeamA(View view){
        viewModel.teamARun = viewModel.teamARun + 4;
        displayScoreTeamA(viewModel.teamARun);
    }
    public void updateSixRunTeamA(View view){
        viewModel.teamARun = viewModel.teamARun + 6;
        displayScoreTeamA(viewModel.teamARun);
    }
    public void updateOneRunTeamA(View view){
        viewModel.teamARun = viewModel.teamARun + 1;
        displayScoreTeamA(viewModel.teamARun);
    }
    public void updateWicketTeamA(View view){
        viewModel.teamAWicket = viewModel.teamAWicket + 1;
        displayScoreTeamA(viewModel.teamARun);
    }
    public void updateBallTeamA(View view){
        viewModel.teamABall = viewModel.teamABall + 1;
        if (viewModel.teamABall ==6){
            updateOverTeamA(view);
            viewModel.teamABall=0;
        }
        displayScoreTeamA(viewModel.teamARun);
    }
    public void updateOverTeamA(View view){
        viewModel.teamAOver=viewModel.teamAOver+1;
        displayScoreTeamA(viewModel.teamARun);
    }
/*team b attributes
* */

    public void updateFourRunTeamB(View view){
        viewModel.teamBRun = viewModel.teamBRun + 4;
        displayScoreTeamB(viewModel.teamBRun);
    }
    public void updateSixRunTeamB(View view){
        viewModel.teamBRun = viewModel.teamBRun + 6;
        displayScoreTeamB(viewModel.teamBRun);
    }
    public void updateOneRunTeamB(View view){
        viewModel.teamBRun = viewModel.teamBRun + 1;
        displayScoreTeamB(viewModel.teamBRun);
    }
    public void updateWicketTeamB(View view){
        viewModel.teamBWicket = viewModel.teamBWicket + 1;
        displayScoreTeamB(viewModel.teamBRun);
    }
    public void updateBallTeamB(View view){
        viewModel.teamBBall = viewModel.teamBBall + 1;
        if (viewModel.teamBBall ==6){
            updateOverTeamB(view);
            viewModel.teamBBall=0;
        }
        displayScoreTeamB(viewModel.teamBRun);
    }
    public void updateOverTeamB(View view){
        viewModel.teamBOver=viewModel.teamBOver+1;
        displayScoreTeamB(viewModel.teamBRun);
    }
    public void resetScoreBoardToInitial(View view){
        viewModel.teamARun=0;
        viewModel.teamAOver=0;
        viewModel.teamAWicket=0;
        viewModel.teamABall=0;
        displayScoreTeamA(viewModel.teamARun);
        viewModel.teamBRun=0;
        viewModel.teamBOver=0;
        viewModel.teamBWicket=0;
        viewModel.teamBBall=0;
        displayScoreTeamB(viewModel.teamBRun);
    }

   /* @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(TEAM_A_RUN,teamARun);

    }*/
}
