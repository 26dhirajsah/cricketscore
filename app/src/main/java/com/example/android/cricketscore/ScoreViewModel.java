package com.example.android.cricketscore;

import android.util.Log;

import androidx.lifecycle.ViewModel;

public class ScoreViewModel extends ViewModel {

    public int teamARun = 0;
    public int teamAWicket = 0;
    public int teamAOver = 0;
    public int teamABall= 0;

    public int teamBRun= 0;
    public int teamBOver= 0;
    public int teamBBall= 0;
    public int teamBWicket= 0;

    public ScoreViewModel(){
        super();
        Log.i("ScoreViewModel","Score View Model Created");
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        Log.i("ScoreViewModel","Score View Model destroyed");
    }
}
